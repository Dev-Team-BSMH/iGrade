import gql from "graphql-tag";

const EvaluationTreeSubjectByCycle = gql`
  query($subjectId: Int, $cycleId: Int) {
    EditingScreenCurrentNode: grades_cycle_subject_instances(
      where: { subject_id: { _eq: $subjectId }, cycle_id: { _eq: $cycleId } }
    ) {
      order
      weight
      node: subject {
        name
        id
        grades_aggregate {
          aggregate {
            count
          }
        }
        children: tests {
          cycle_instances(
            order_by: { order: asc }
            where: { cycle_id: { _eq: $cycleId } }
          ) {
            weight
            order
            child: test {
              id
              name
            }
          }
        }
      }
    }
  }
`;
export default EvaluationTreeSubjectByCycle;
