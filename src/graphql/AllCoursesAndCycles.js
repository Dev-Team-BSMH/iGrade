import gql from "graphql-tag";
const AllCoursesAndCycles = gql`
  query {
    courses: users_courses {
      id
      name
      cycles {
        id
        name
        course_id
      }
    }
  }
`;
export default AllCoursesAndCycles;
