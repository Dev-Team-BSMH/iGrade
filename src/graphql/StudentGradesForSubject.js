import gql from "graphql-tag";
const StudentGradesForSubject = gql`
  query($cycleId: Int, $subjectId: Int) {
    grades: grades_student_subject_grades(
      where: {
        student: { department: { cycle_id: { _eq: $cycleId } } }
        subject_id: { _eq: $subjectId }
      }
    ) {
      student {
        student_number
        user {
          first_name
          last_name
        }
        department {
          number
        }
      }
      grade
    }
  }
`;
export default StudentGradesForSubject;
