import gql from "graphql-tag";

const EvaluationTreeDisplay = gql`
  query($cycleId: Int) {
    tree: grades_cycle_subject_instances(
      where: { cycle_id: { _eq: $cycleId } }
      order_by: { order: asc }
    ) {
      order
      weight
      subject {
        id
        name
        tests(where: { cycle_instances: { cycle_id: { _eq: $cycleId } } }) {
          cycle_instances(
            order_by: { order: asc }
            where: { cycle_id: { _eq: $cycleId } }
          ) {
            test {
              id
              name
              components(
                where: { cycle_instances: { cycle_id: { _eq: $cycleId } } }
              ) {
                cycle_instances(
                  order_by: { order: asc }
                  where: { cycle_id: { _eq: $cycleId } }
                ) {
                  component {
                    id
                    name
                  }
                  order
                  weight
                }
              }
            }
            order
            weight
          }
        }
      }
    }
  }
`;
export default EvaluationTreeDisplay;
