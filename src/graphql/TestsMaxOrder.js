import gql from "graphql-tag";

const TestsMaxOrder = gql`
  query($cycleId: Int, $subjectId: Int) {
    testsMaxOrder: grades_cycle_test_instances_aggregate(
      where: {
        cycle_id: { _eq: $cycleId }
        _and: { test: { subject_id: { _eq: $subjectId } } }
      }
    ) {
      aggregate {
        max {
          order
        }
      }
    }
  }
`;
export default TestsMaxOrder;
