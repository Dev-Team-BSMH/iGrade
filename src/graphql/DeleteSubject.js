import gql from "graphql-tag";

const DeleteSubject = gql`
  mutation delete_subject($subjectId: Int, $cycleId: Int) {
    delete_grades_subjects(
      where: {
        id: { _eq: $subjectId }
        cycle_instances: { cycle_id: { _eq: $cycleId } }
      }
    ) {
      affected_rows
    }
  }
`;
export default DeleteSubject;
