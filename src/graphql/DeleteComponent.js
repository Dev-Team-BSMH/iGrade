import gql from "graphql-tag";

const DeleteComponent = gql`
  mutation delete_component($componentId: Int, $cycleId: Int) {
    delete_grades_components(
      where: {
        id: { _eq: $componentId }
        cycle_instances: { cycle_id: { _eq: $cycleId } }
      }
    ) {
      affected_rows
    }
  }
`;
export default DeleteComponent;
