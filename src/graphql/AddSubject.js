import gql from "graphql-tag";

const AddSubject = gql`
  mutation update_subject_weight_and_name(
    $name: String
    $weight: Int
    $cycleId: Int
    $order: smallint
    $courseId: Int
  ) {
    insert_grades_subjects(
      objects: [
        {
          name: $name
          course_id: $courseId
          cycle_instances: {
            data: { cycle_id: $cycleId, weight: $weight, order: $order }
            on_conflict: {
              constraint: subject_and_cycle_unique
              update_columns: [weight]
            }
          }
        }
      ]
      on_conflict: { constraint: subjects_pkey, update_columns: [name] }
    ) {
      affected_rows
    }
  }
`;
export default AddSubject;
