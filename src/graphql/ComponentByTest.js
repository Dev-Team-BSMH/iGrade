import gql from "graphql-tag";
const ComponentByTest = gql`
  query($testId: Int, $cycleId: Int) {
    test_components: grades_cycle_component_instances(
      where: {
        component: { test_id: { _eq: $testId } }
        _and: { cycle_id: { _eq: $cycleId } }
      }
      order_by: { order: asc }
    ) {
      id
      component_id
      component {
        name
      }
      order
    }
  }
`;
export default ComponentByTest;
