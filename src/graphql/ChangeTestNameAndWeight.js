import gql from "graphql-tag";
const ChangeTestNameAndWeight = gql`
  mutation update_test_weight_and_name(
    $name: String
    $weight: Int
    $cycleId: Int
    $testId: Int
    $subjectId: Int
    $order: smallint
  ) {
    insert_grades_tests(
      objects: [
        {
          id: $testId
          name: $name
          subject_id: $subjectId
          cycle_instances: {
            data: { cycle_id: $cycleId, weight: $weight, order: $order }
            on_conflict: {
              constraint: test_and_cycle_unique
              update_columns: [weight]
            }
          }
        }
      ]
      on_conflict: { constraint: tests_pkey, update_columns: [name] }
    ) {
      affected_rows
    }
  }
`;
export default ChangeTestNameAndWeight;
