import gql from "graphql-tag";

const InsertGradeComponent = gql`
  mutation insert_grade_for_student_component(
    $sid: Int
    $gr: Float
    $cid: Int
  ) {
    insert_grades_grades(
      objects: [{ student_id: $sid, grade: $gr, component_id: $cid }]
      on_conflict: { constraint: student_and_component, update_columns: grade }
    ) {
      affected_rows
    }
  }
`;
export default InsertGradeComponent;
