import gql from "graphql-tag";

const AddTest = gql`
  mutation add_test(
    $name: String
    $weight: Int
    $cycleId: Int
    $subjectId: Int
    $order: smallint
  ) {
    insert_grades_tests(
      objects: [
        {
          name: $name
          subject_id: $subjectId
          cycle_instances: {
            data: { cycle_id: $cycleId, weight: $weight, order: $order }
            on_conflict: {
              constraint: test_and_cycle_unique
              update_columns: [weight]
            }
          }
        }
      ]
      on_conflict: { constraint: tests_pkey, update_columns: [name] }
    ) {
      affected_rows
    }
  }
`;
export default AddTest;
