import Vue from "vue";
import VueRouter from "vue-router";
import DisplayGradesPage from "./views/DisplayGradesPage";
import HomePage from "./views/HomePage";
import EvaluationTreePage from "./views/EvaluationTreePage";
import InsertingGradesPage from "./views/InsertingGradesPage";
import StudentGradesForCourse from "./graphql/StudentGradesForCourse";
import StudentGradesForSubject from "./graphql/StudentGradesForSubject";
import StudentGradesForTest from "./graphql/StudentGradesForTest";
import StudentGradesForComponent from "./graphql/StudentGradesForComponent";
import EvaluationTreeTestByCycle from "./graphql/EvaluationTreeTestByCycle";
import EvaluationTreeSubjectByCycle from "./graphql/EvaluationTreeSubjectByCycle";
import EvaluationTreeComponentByCycle from "./graphql/EvaluationTreeComponentByCycle";
import GradesSheet from "./views/GradesSheet";

Vue.use(VueRouter);

export default new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "HomePage",
      component: HomePage
    },
    {
      path: "/course/:courseId",
      name: "HomePageCourse",
      props: true,
      component: HomePage
    },
    {
      path: "/course/:courseId/cycle/:cycleId",
      name: "DisplayGradesCycle",
      props: route => ({
        query: StudentGradesForCourse,
        variables: {
          cycleId: route.params.cycleId,
          courseId: route.params.courseId
        }
      }),
      component: DisplayGradesPage
    },
    {
      path: "/course/:courseId/cycle/:cycleId/subject/:subjectId",
      name: "DisplayGradesSubject",
      props: route => ({
        query: StudentGradesForSubject,
        variables: {
          cycleId: route.params.cycleId,
          subjectId: route.params.subjectId
        }
      }),
      component: DisplayGradesPage
    },
    {
      path: "/course/:courseId/cycle/:cycleId/subject/:subjectId/test/:testId",
      name: "DisplayGradesTest",
      props: route => ({
        query: StudentGradesForTest,
        variables: {
          cycleId: route.params.cycleId,
          testId: route.params.testId
        }
      }),
      component: DisplayGradesPage
    },
    {
      path:
        "/course/:courseId/cycle/:cycleId/subject/:subjectId/test/:testId/component/:componentId",
      name: "DisplayGradesComponent",
      props: route => ({
        query: StudentGradesForComponent,
        variables: {
          cycleId: route.params.cycleId,
          componentId: route.params.componentId
        }
      }),
      component: DisplayGradesPage
    },
    {
      name: "EvaluationTree",
      path: "/evaluationTree/course/:courseId/cycle/:cycleId",
      component: EvaluationTreePage
    },
    {
      name: "EvaluationTreeSubjectByCycle",
      path:
        "/evaluationTree/course/:courseId/cycle/:cycleId/subject/:subjectId",
      props: route => ({
        query: EvaluationTreeSubjectByCycle,
        variables: {
          subjectId: route.params.subjectId,
          cycleId: route.params.cycleId
        }
      }),
      component: EvaluationTreePage
    },
    {
      name: "EvaluationTreeTestByCycle",
      path:
        "/evaluationTree/course/:courseId/cycle/:cycleId/subject/:subjectId/test/:testId",
      props: route => ({
        query: EvaluationTreeTestByCycle,
        variables: {
          testId: route.params.testId,
          cycleId: route.params.cycleId
        }
      }),
      component: EvaluationTreePage
    },
    {
      name: "EvaluationTreeComponentByCycle",
      path:
        "/evaluationTree/course/:courseId/cycle/:cycleId/subject/:subjectId/test/:testId/component/:componentId",
      props: route => ({
        query: EvaluationTreeComponentByCycle,
        variables: {
          componentId: route.params.componentId,
          cycleId: route.params.cycleId
        }
      }),
      component: EvaluationTreePage
    },
    {
      name: "InsertingGradesPage",
      path:
        "/insertingGrades/course/:courseId/cycle/:cycleId/subject/:subjectId/test/:testId",
      component: InsertingGradesPage
    },
    {
      name: "GenerateGradesSheet",
      path: "/GenerateGradesSheet/course/:courseId/cycle/:cycleId",
      component: GradesSheet
    }
  ]
});
